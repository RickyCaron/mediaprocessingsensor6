#include "sendaction.h"

SendAction::SendAction(string receiver,string message) :receiver(receiver),message(message){}

void SendAction::triggered()
{
    cout<<"Send "<<message<<"to "<<receiver<<" !"<<endl;
}



void SendAction::setReceiver(const string &value)
{
    receiver = value;
}

string SendAction::getMessage() const
{
    return message;
}

void SendAction::setMessage(const string &value)
{
    message = value;
}

void SendAction::printAllInformation()
{
    std::cout<<"Sendaction of receiver "<<receiver<<" and message "<<message<<" ";
}

string SendAction::getAllInformation()
{
    return "Sendaction of receiver "+ receiver +" and message "+ message + " ";
}

string SendAction::getReceiver() const
{
    return receiver;
}

#include "alarmaction.h"
//#include <iostream>


AlarmAction::AlarmAction(string alarm):alarm(alarm)
{

}

void AlarmAction::setAlarm(const string &value)
{
    alarm = value;
}

void AlarmAction::printAllInformation()
{
    std::cout<<"Alarm action to alarm "<< alarm<< " ";
}

string AlarmAction::getAllInformation()
{
    return "Alarm action to alarm "+ alarm + " ";
}

string AlarmAction::getAlarm()
{
    return alarm;
}

void AlarmAction::triggered(){
 cout<<"Activating the "<< alarm <<" ."<<endl;
}

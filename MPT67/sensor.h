#ifndef SENSOR_H
#define SENSOR_H
#include "emergencyaction.h"
#include <iostream>
#include <vector>
#include <iterator>
#include <utility>
#include <sstream>
#include <memory>
using namespace std::rel_ops;

enum SensorType{ST_Undefined,ST_SmokeSensor,ST_MotionSensor,ST_GasSensor};

class Sensor
{
protected:
    static int sensorCount;//the number of all exsicting sensors
    static int sensorUniqCount;// the number records how many sensors have been created, some of which might have been destroyes
    int id = 0;//automatically generated, equals to sensorUniqCount
    SensorType sensorType=ST_Undefined;// initialized when created
    string vender;
    bool activationState=false;
    //EmergencyAction * emergencyActions = nullptr;
    vector < shared_ptr<EmergencyAction> > emergencyActions;// a vesctor storing shared pointers to emergency action

public:

    Sensor(SensorType,string);
    ~Sensor();
//    static Sensor* CreateSensor(SensorType,string); // It seems not necessary
    virtual bool activate()=0;//activate the sensor, then it can be triggered
    void deactivate();//deactivate the sensor
    void triggered();//triggered the sensor and all emergency actions in it will be called
    Sensor& operator ++();//prefix increment, activate the sensor
    //Sensor operator++(int);  //post increment operator
    Sensor& operator--();//deactivate the sensor
    //Sensor operator--(int);

    bool addEmergrncyAction(shared_ptr<EmergencyAction>);
    bool removeEmergencyAction(shared_ptr<EmergencyAction>);
    bool removeAllEmergencyAction();
    virtual void printAllInformation()=0;
    virtual string getAllInformation()=0;

    static int getSensorCount();
    static void setSensorCount(int value);
    static int getSensorUniqCount();
    static void setSensorUniqCount(int value);
};

 ostream&  operator<<(ostream &os, Sensor &sensor);

#endif // SENSOR_H

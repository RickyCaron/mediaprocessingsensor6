#include "smokesensor.h"

SmokeSensor::SmokeSensor(SensorType sensorType, string vender): Sensor(sensorType,vender)
{

}

string SmokeSensor::getAllInformation()
{
    std::stringstream sensorInformation;
    sensorInformation <<"Smoke sensor of id "<<id<<" is produce by "<<vender<<"is ";
    if(activationState)
       sensorInformation<<"active now! ";
    else
        sensorInformation<<"not active now! ";
    return sensorInformation.str();
}

void SmokeSensor::printAllInformation()
{
    cout<<"Smoke sensor of id "<<id<<" is produce by "<<vender<<"is ";
    if(activationState)
        cout<<"active now! ";
    else
        cout<<"not active now! ";
    cout<<endl;
}

bool SmokeSensor::activate()
{
    std::cout<<"Now try to activate Smoke Sensor of id "<< id<<endl;
    activationState=true;
    return activationState;
}

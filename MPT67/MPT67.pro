QT -= gui

TEMPLATE = lib
DEFINES += MPT67_LIBRARY

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    alarmaction.cpp \
    emergencyaction.cpp \
    emergencycenter.cpp \
    gassensor.cpp \
    group.cpp \
    motionsensor.cpp \
    mpt67.cpp \
    sendaction.cpp \
    sensor.cpp \
    single.cpp \
    smokesensor.cpp \
    unit.cpp \
    warnaction.cpp

HEADERS += \
    MPT67_global.h \
    alarmaction.h \
    emergencyaction.h \
    emergencycenter.h \
    gassensor.h \
    group.h \
    motionsensor.h \
    mpt67.h \
    sendaction.h \
    sensor.h \
    single.h \
    smokesensor.h \
    unit.h \
    warnaction.h

# Default rules for deployment.
unix {
    target.path = /usr/lib
}
!isEmpty(target.path): INSTALLS += target

#ifndef WARNACTION_H
#define WARNACTION_H

#include"emergencyaction.h"
class WarnAction: public EmergencyAction
{
private:
    string warner;
public:
    WarnAction(string);
    void triggered();
    string getWarner() const;
    void setWarner(const string &value);
    void printAllInformation();
    string getAllInformation();
};

#endif // WARNACTION_H

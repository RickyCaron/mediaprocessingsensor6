#include "warnaction.h"

string WarnAction::getWarner() const
{
    return warner;
}

void WarnAction::setWarner(const string &value)
{
    warner = value;
}

void WarnAction::printAllInformation()
{
    std::cout<<"Warn action to warn "<<warner<< ' ';
}

string WarnAction::getAllInformation()
{
    return "Warn action to warn " + warner + ' ';
}

WarnAction::WarnAction(string warner):warner(warner){}

void WarnAction::triggered()
{
    cout<<"Warn "<<warner<<" !"<<endl;
}
